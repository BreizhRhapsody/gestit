<?php

namespace App\Controller\Admin;

use App\Entity\Lists;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ListsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Lists::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('label'),
            TextEditorField::new('description'),
            TextField::new('color'),
        ];
    }
}
