<?php

namespace App\Controller;

use App\Repository\ListsRepository;
use App\Repository\TaskRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'app_home')]
    public function index(
        ListsRepository $listsRepository,
        TaskRepository $taskRepository,
        PaginatorInterface $paginator,
        Request $request,
    ): Response {
        $data = $listsRepository->findAll();

        $lists = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );


        return $this->render('home/index.html.twig', [
            'lists' => $lists,
            'tasks' => $taskRepository->findAll(),
        ]);
    }
}
