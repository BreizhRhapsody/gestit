<?php

namespace App\EventSubscriber;

use App\Entity\Lists;
use Doctrine\Migrations\Tools\Console\Command\StatusCommand;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\Security\Core\Security;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setListsUser'],
        ];
    }

    public function setListsUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if (!($entity instanceof Lists)) {
            return;
        }

        $user = $this->security->getUser();
        $entity->setUser($user);
    }
}
