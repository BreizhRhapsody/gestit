<?php

namespace App\DataFixtures;

use App\Entity\Lists;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        // Création d'un utilisateur
        $user = new User();

        $user->setUsername('j-doe')
             ->setFirstname('John')
             ->setLastname('Doe');

        $password = $this->encoder->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);

        // Création de listes
        for ($i=0; $i < 10; $i++) {
            $lists = new Lists();

            $lists->setLabel('Test listes')
                  ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                  Suspendisse eu magna vel tellus accumsan blandit.')
                  ->setColor('info')
                  ->setUser($user);


            $manager->persist($lists);
        }

        // Création de listes pour les tests
        $lists = new Lists();

        $lists->setLabel('Listes test')
              ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
              Suspendisse eu magna vel tellus accumsan blandit.')
              ->setUser($user)
              ->setColor('info');
        
              $manager->persist($lists);

        // Création de tâches
        $task = new Task();

        $task->setLabel('Test tâches')
             ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
             Suspendisse eu magna vel tellus accumsan blandit.')
             ->setStatus(true)
             ->setUser($user);

        $manager->persist($task);

        // Création de tâches pour les tests
        $task = new Task();

        $task->setLabel('Tâches test')
             ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
             Suspendisse eu magna vel tellus accumsan blandit.')
             ->setUser($user)
             ->setStatus(true);

        $manager->persist($task);
                
        $manager->flush();
    }
}
