<?php

namespace App\Tests;

use App\Entity\Task;
use App\Entity\Lists;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class TaskUnitTest extends TestCase
{
    public function testIsTrue() : void
    {
        $task = new Task();
        $lists = new Lists();
        $user = new User();

        $task->setLabel('label')
             ->setDescription('description')
             ->setStatus(true)
             ->addList($lists)
             ->setUser($user);
             
        $this->assertTrue($task->getLabel() === 'label');
        $this->assertTrue($task->getDescription() === 'description');
        $this->assertTrue($task->isStatus() === true);
        $this->assertContains($lists, $task->getLists());
        $this->assertTrue($task->getUser() === $user);
    }

    public function testIsFalse() : void
    {
        $task = new Task();
        $lists = new Lists();
        $user = new User();

        $task->setLabel('label')
             ->setDescription('description')
             ->setStatus(true)
             ->addList($lists)
             ->setUser($user);
             
        $this->assertFalse($task->getLabel() === 'false');
        $this->assertFalse($task->getDescription() === 'false');
        $this->assertFalse($task->isStatus() === false);
        $this->assertNotContains(new Lists(), $task->getLists());
        $this->assertFalse($task->getUser() === new User());
    }       

    public function testIsEmpty() : void 
    {
        $task = new Task();

        $this->assertEmpty($task->getLabel());
        $this->assertEmpty($task->getDescription());
        $this->assertEmpty($task->isStatus());
        $this->assertEmpty($task->getLists());
        $this->assertEmpty($task->getId());
    }
}
