<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateListFunctionalTest extends WebTestCase
{
    public function testShouldDisplayCreateList(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/create-list');

        $this->assertResponseIsSuccessful();
    }
}