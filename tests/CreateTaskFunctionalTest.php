<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateTaskFunctionalTest extends WebTestCase
{
    public function testShouldDisplayCreateTask(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/create-task');

        $this->assertResponseIsSuccessful();
    }
}
