<?php

namespace App\Tests;

use App\Entity\Lists;
use App\Entity\Task;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();

        $user->setUsername('username');
        $user->setPassword('password');
        $user->setFirstname('firstname');
        $user->setLastname('lastname');
        $user->setRoles(['ROLE_TEST']);
    
        $this->assertTrue($user->getUsername() === 'username');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getFirstname() === 'firstname');
        $this->assertTrue($user->getLastname() === 'lastname');
        $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);
        $this->assertTrue($user->getUserIdentifier() === 'username');
    }
    
    public function testIsFalse()
    {
        $user = new User();

        $user->setUsername('username');
        $user->setPassword('password');
        $user->setFirstname('firstname');
        $user->setLastname('lastname');

        $this->assertFalse($user->getUsername() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getUserIdentifier() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getUsername());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getId());
    }

    public function testAddGetRemoveLists() 
    {
        $user = new User();
        $lists = new Lists();

        $this->assertEmpty($user->getLists());

        $user->addList($lists);
        $this->assertContains($lists, $user->getLists());

        $user->removeList($lists);
        $this->assertEmpty($user->getLists());
    }

    public function testAddGetRemoveTask() 
    {
        $user = new User();
        $task = new Task();

        $this->assertEmpty($user->getTask());

        $user->addTask($task);
        $this->assertContains($task, $user->getTask());

        $user->removeTask($task);
        $this->assertEmpty($user->getTask());
    }
}    
