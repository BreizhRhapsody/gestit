<?php

namespace App\Tests;

use App\Entity\Task;
use App\Entity\Lists;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ListsUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $lists = new Lists();
        $user = new User();

        $lists->setLabel('label');
        $lists->setDescription('description');
        $lists->setColor('color');
        $lists->setUser($user);

        $this->assertTrue($lists->getLabel() === 'label');
        $this->assertTrue($lists->getDescription() === 'description');
        $this->assertTrue($lists->getColor() === 'color');
        $this->assertTrue($lists->getUser() === $user);
    }
    
    public function testIsFalse()
    {
        $lists = new Lists();
        $user = new User();

        $lists->setLabel('label');
        $lists->setDescription('description');
        $lists->setColor('color');
        $lists->setUser($user);

        $this->assertFalse($lists->getLabel() === 'false');
        $this->assertFalse($lists->getDescription() === 'false');
        $this->assertFalse($lists->getColor() === 'false');
        $this->assertFalse($lists->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $lists = new Lists();

        $this->assertEmpty($lists->getLabel());
        $this->assertEmpty($lists->getDescription());
        $this->assertEmpty($lists->getColor());
        $this->assertEmpty($lists->getId());
    }

    public function testAddGetRemoveLists()
    {
        $lists = new Lists();
        $task = new Task();

        $this->assertEmpty($lists->getTasks());

        $lists->addTask($task);
        $this->assertContains($task, $lists->getTasks());

        $lists->removeTask($task);
        $this->assertEmpty($lists->getTasks());
    }
}
