# GEST-IT

Web application to manage your to-do lists created during my training at Study

## Development environment

### Prerequisite

* PHP 8.1.5
* Composer
* Symfony CLI
* Docker
* Docker-compose
* Nodejs and npm

You can check the prerequisites (except Docker and Docker-compose) with the following command (from Symfony CLI):

```bash
symfony check:requirements
```
### Launch the development environment

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```

### Add data for tests

```bash
symfony console doctrine:fixtures:load
```

## Launch tests

```bash
php bin/phpunit --testdox
```
